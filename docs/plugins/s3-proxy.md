---
layout: docs
title: s3-proxy
menu: docs
submenu: plugins
lang: en
---

The [4front-s3-proxy](https://github.com/4front/s3-proxy) plugin provides a way to access files stored in an S3 bucket directly from the browser. This could be via a simple `<image/>` tag or JavaScript AJAX calls. The plugin will honor any cache headers set in the S3 metadata, or you can configure the plugin to override the cache headers. The origin S3 bucket does _not_ need to be configured for web hosting.

## Basic Configuration
~~~json
{
  "_virtualApp": {
    "router": [
      {
        "path": "/media",
        "module": "4front-s3-proxy",
        "options": {
          "bucket": "bucket-name",
          "accessKeyId": "env:S3_ACCESS_KEY_ID",
          "secretAccessKey": "env:S3_SECRET_ACCESS_KEY",
          "overrideCacheControl": "max-age=10000"
        }
      }
    ]
  }
}
~~~

### Options

__`accessKeyId`__

The AWS access key of the IAM user to connect to S3 with  (environment variable recommended).

__`secretAccessKey`__

The AWS secret access key (environment variable recommended).

__`region`__

The AWS region of the bucket, i.e. "us-west-2".

__`bucket`__

The name of the S3 bucket.

__`prefix`__

Optional path to the root S3 folder where the files to be hosted live. If omitted, the http requests to the proxy need to mirror the full S3 path.

__`defaultCacheControl`__

Value of the `Cache-Control` header to use if the metadata from the S3 object does not specify it's own value.

__`overrideCacheControl`__

Value of the `Cache-Control` header that is applied to the response even if there there is a different value on the S3 object metadata.

## Sample Setup

Let's assume there is a bucket "mycompany-media-assets". Within this bucket is a folder named "website" where the images, videos, etc. for the company website reside.

~~~sh
mycompany-media-assets
└── website
    └── images
        ├── logo.png
        └── background.jpg
~~~

The corresponding s3-proxy plugin configuration would look something like below. The `Cache-Control` response header will be set to have a max age of 30 days (2592000 seconds) no matter what metadata exists on the corresponding S3 object. This means whatever tool is being used to write the files to S3 doesn't have to worry about configuring proper cache metadata, the proxy will take care of that.

~~~json
{
  "path": "/media",
  "module": "4front-s3-proxy",
  "options": {
    "bucket": "mycompany-media-assets",
    "accessKeyId": "env:S3_ACCESS_KEY_ID",
    "secretAccessKey": "env:S3_SECRET_ACCESS_KEY",
    "prefix": "website",
    "overrideCacheControl": "max-age=10000"
  }
}
~~~

Once the plugin is declared in the package.json manifest, images can be declared in HTML like so:

~~~html
<img src="/media/images/logo.png"/>
~~~

## Advantages

There are several advantages to serving images and binary assets through the s3-proxy rather than deploying them like other static assets:

#### __Faster deployments__

By not re-deploying images with every `4front deploy` command deployments can potentially be much faster due to having far fewer bits to shuffle around.

#### __Less repo bloat__
Moving your images out of your main code repo keeps your repository focused on what `git` and other version control systems do best - tracking changes to text files.

#### __Better caching__

When images reside in the repo each 4front deployment results in a new URL that incorporates the unique versionId, i.e. `/<app_id>/<version_id>/images/brand.jpg`. This is important for `.html`, `.js`, `.css`, etc. where the contents may have changed since the last deployment. The unique URL per version ensures that all clients get the latest source files and not a stale copy from cache.

Images and other binary files, on the other hand, frequently don't change once created. By constantly changing the URLs of images from version to version many of the caching benefits, on the CDN and browser, is negated. This is particularly important for images given that they are often the largest downloads on a site.

With the S3 proxy, image URLs remain constant from version to version so any cached copies remain valid. If you can ensure that file contents never change, you can set an aggressive `overrideCacheControl` setting. In this scenario changes to existing files should be saved with a new name resulting in a new URL.

### HTTP Cache Headers

The `4front-s3-proxy` provides two different caching mechanisms. First you can specify either the `defaultCacheControl` or `overrideCacheControl` options to control the `Cache-Control` header that is sent in the proxied response. The most optimal policy is to specify a `max-age=seconds` value that informs the browser and any intermediary CDN and network proxies to cache the response for the specified number of seconds and not return to the origin server until that time has elapsed.

Secondly it supports the `ETag` value that S3 automatically creates whenever an object is written. The proxy forwards this header along in the http response. If the value of an incoming `If-None-Match` request header matches the `ETag` of the S3 object, the proxy returns an empty `304 Not Modified` response. This is known as a "conditional get" request.

For a more in-depth description of the different caching headers and techniques, see the [Google Developer HTTP caching documentation](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching?hl=en).

### IAM Configuration
It is recommended that a dedicated IAM user be created in the AWS account where the bucket exists that has the minimum necessary rights. The S3 proxy only needs the `s3:GetObject` permission for the path where the hosted assets reside. In the sample setup described above, the IAM policy for the user would look something like so:

~~~json
{
   "Version": "2015-11-17",
   "Id": "123",
   "Statement": [
     {
       "Sid": "",
       "Effect": "Allow",
       "Principal": "*",
       "Action": "s3:GetObject",
       "Resource": "arn:aws:s3:::mycompany-media-assets/website/*",
     }
   ]
}
~~~

If the S3 bucket is in the same AWS account as where 4front is running, it's not necessary to specify the `accessKeyId` and `secretAccessKey`. Instead the IAM role of the 4front Elastic Beanstalk environment should be granted access to the bucket. The plugin will intrinsically make the call to S3 as this IAM role.

## S3 as a database

Another potential use for the S3 proxy is to use S3 as a lightweight and affordable database using something like tab-delimited, xml, or json flat files. Client JavaScript would make AJAX calls to the proxy to pull down datasets into browser memory. The previously discussed caching benefits also yield perf gains in this scenario.

~~~js
$.ajax({
  url: '/media/datasets/sales-12-2015.json',
  contentType: 'application/json',
  success: function(data) {
    // do something interesting with the data
  }
});
~~~
